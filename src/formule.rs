use crate::rawformule::RawFormule;
use crate::valuation::TranslatedValuation;
use crate::varmap::VariableMap;
#[derive(Debug, Clone)]
pub struct ParseFormuleError {
    desc: String,
}
impl std::fmt::Display for ParseFormuleError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.desc)
    }
}
impl std::error::Error for ParseFormuleError {}
#[derive(Clone)]
pub struct Formule {
    inner: RawFormule,
    varmap: VariableMap,
}
impl std::ops::Not for Formule {
    type Output = Formule;

    fn not(self) -> Self::Output {
        Self {
            inner: !self.inner,
            varmap: self.varmap,
        }
    }
}
impl Formule {
    pub fn satisfiable(&self) -> Option<TranslatedValuation> {
        self.inner.satisfiable().map(|v| v.translate(&self.varmap))
    }
    pub fn simplify(self) -> Self {
        Self {
            inner: self.inner.simplify(),
            varmap: self.varmap,
        }
    }
}
impl std::str::FromStr for Formule {
    type Err = ParseFormuleError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut varmap = VariableMap::default();
        let formule = crate::grammar::ExprParser::new()
            .parse(&mut varmap, s)
            .map_err(|err| ParseFormuleError {
                desc: err.to_string(),
            })?;
        Ok(Formule {
            inner: formule,
            varmap,
        })
    }
}
impl std::fmt::Display for Formule {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.inner.translate(&self.varmap))
    }
}
