use crate::valuation::Valuation;
use crate::{quine::solve, varmap::VariableMap};
use std::{
    collections::BTreeSet,
    ops::{BitAnd, BitOr, Not},
};

#[derive(Clone, Debug)]
pub enum RawFormule {
    Top,
    Bottom,
    Not(Box<RawFormule>),
    Conj(Vec<RawFormule>),
    Disj(Vec<RawFormule>),
    Var(u32),
    Imply(Box<RawFormule>, Box<RawFormule>),
    Equiv(Box<RawFormule>, Box<RawFormule>),
}

// Si b est vrai, met x entre parenthèses
macro_rules! paren {
    ($b:expr, $x:expr) => {
        if $b {
            format!("({})", $x)
        } else {
            $x
        }
    };
}

impl BitAnd for RawFormule {
    type Output = RawFormule;

    fn bitand(self, rhs: Self) -> Self::Output {
        Self::Conj([self, rhs].into())
    }
}
impl BitOr for RawFormule {
    type Output = RawFormule;

    fn bitor(self, rhs: Self) -> Self::Output {
        Self::Disj([self, rhs].into())
    }
}
impl Not for RawFormule {
    type Output = RawFormule;

    fn not(self) -> Self::Output {
        match self {
            Self::Not(a) => *a,
            f => Self::Not(f.into()),
        }
    }
}
impl PartialEq for RawFormule {
    // Renvoie true si self et other sont équivalents
    fn eq(&self, other: &Self) -> bool {
        solve(Self::Disj(
            [
                Self::Conj([self.to_owned().into(), (!other.to_owned()).into()].into()),
                Self::Conj([other.to_owned().into(), (!self.to_owned()).into()].into()),
            ]
            .into(),
        ))
        .is_none()
    }
}
impl Eq for RawFormule {}
impl RawFormule {
    pub fn satisfiable(&self) -> Option<Valuation> {
        solve(self.clone())
    }
    pub fn list_vars(&self, vars: &mut BTreeSet<u32>) {
        match self {
            Self::Conj(l) | Self::Disj(l) => {
                for f in l {
                    f.list_vars(vars);
                }
            }
            Self::Not(f) => {
                f.list_vars(vars);
            }
            Self::Imply(x, y) => {
                x.list_vars(vars);
                y.list_vars(vars);
            }
            Self::Var(v) => {
                vars.insert(*v);
            }
            _ => {}
        }
    }

    pub fn eval(&self, vars: &Valuation) -> Option<bool> {
        let mut local = BTreeSet::new();
        self.list_vars(&mut local);
        if local.into_iter().all(|x| vars.get(x).is_some()) {
            Some(self.eval_unsafe(vars))
        } else {
            None // Valuation incomplète
        }
    }
    fn eval_unsafe(&self, vars: &Valuation) -> bool {
        match self {
            Self::Not(f) => !f.eval_unsafe(vars),
            Self::Var(x) => vars.get(*x).unwrap(),
            Self::Disj(l) => l.iter().any(|f| f.eval_unsafe(vars)),
            Self::Conj(l) => l.iter().all(|f| f.eval_unsafe(vars)),
            Self::Top => true,
            Self::Bottom => false,
            Self::Imply(x, y) => !x.eval_unsafe(vars) || y.eval_unsafe(vars),
            Self::Equiv(x, y) => x.eval_unsafe(vars) == y.eval_unsafe(vars),
        }
    }
    pub fn replace(self, a: u32, b: &RawFormule) -> Self {
        match self {
            Self::Not(f) => Self::Not(f.replace(a, b).into()),
            Self::Conj(f) => Self::Conj(f.into_iter().map(|e| e.replace(a, b)).collect()),
            Self::Disj(f) => Self::Disj(f.into_iter().map(|e| e.replace(a, b)).collect()),
            Self::Imply(x, y) => Self::Imply(x.replace(a, b).into(), y.replace(a, b).into()),
            Self::Equiv(x, y) => Self::Equiv(x.replace(a, b).into(), y.replace(a, b).into()),
            Self::Var(v) if v == a => b.to_owned(),
            _ => self,
        }
    }
    pub fn simplify(self) -> Self {
        match self {
            Self::Not(f) => match f.simplify() {
                Self::Not(g) => *g,
                Self::Imply(x, y) => Self::Conj([x.simplify(), (!*y).simplify()].into()),
                Self::Equiv(x, y) => {
                    let x = x.simplify();
                    let y = y.simplify();
                    Self::Disj(
                        [
                            Self::Conj([x.to_owned().into(), (!y.to_owned()).into()].into()),
                            Self::Conj([y.to_owned().into(), (!x.to_owned()).into()].into()),
                        ]
                        .into(),
                    )
                }
                Self::Top => Self::Bottom,
                Self::Bottom => Self::Top,
                f => f.not(),
            },
            Self::Disj(v) => {
                let v2 = v.into_iter().fold(Some(Vec::new()), |accu, x| {
                    accu.and_then(|mut accu| match x.simplify() {
                        Self::Bottom => Some(accu),
                        Self::Top => None,
                        Self::Disj(l) => {
                            accu.extend(l.into_iter().map(|e| e.simplify()));
                            Some(accu)
                        }
                        x => {
                            accu.push(x);
                            Some(accu)
                        }
                    })
                });
                if let Some(mut v) = v2 {
                    if v.is_empty() {
                        Self::Bottom
                    } else if v.len() == 1 {
                        v.swap_remove(0)
                    } else {
                        Self::Disj(v)
                    }
                } else {
                    Self::Top
                }
            }
            Self::Conj(v) => {
                let v2 = v.into_iter().fold(Some(Vec::new()), |accu, x| {
                    accu.and_then(|mut accu| match x.simplify() {
                        Self::Bottom => None,
                        Self::Top => Some(accu),
                        Self::Conj(l) => {
                            accu.extend(l.into_iter().map(|e| e.simplify()));
                            Some(accu)
                        }
                        x => {
                            accu.push(x);
                            Some(accu)
                        }
                    })
                });
                if let Some(mut v) = v2 {
                    if v.is_empty() {
                        Self::Top
                    } else if v.len() == 1 {
                        v.swap_remove(0)
                    } else {
                        Self::Conj(v)
                    }
                } else {
                    Self::Bottom
                }
            }
            Self::Imply(x, y) => Self::Disj([!*x, *y].to_vec()).simplify(),
            Self::Equiv(x, y) => Self::Disj(
                [
                    Self::Conj([(*x).to_owned().into(), (*y).to_owned().into()].to_vec()),
                    Self::Conj([(!*x).to_owned().into(), (!*y).to_owned().into()].to_vec()),
                ]
                .to_vec(),
            )
            .simplify(),
            _ => self,
        }
    }
    pub fn translate(&self, varmap: &VariableMap) -> String {
        match self {
            Self::Top => "\u{22A4}".to_owned(),
            Self::Bottom => "\u{22A5}".to_owned(),
            Self::Not(fo) => {
                format!(
                    "\u{00ac}{}",
                    paren!(
                        matches!(
                            **fo,
                            Self::Conj(_) | Self::Disj(_) | Self::Imply(_, _) | Self::Equiv(_, _)
                        ),
                        fo.translate(varmap)
                    )
                )
            }
            Self::Disj(l) => l
                .iter()
                .map(|fo| {
                    paren!(
                        matches!(*fo, Self::Conj(_) | Self::Imply(_, _) | Self::Equiv(_, _)),
                        fo.translate(varmap)
                    )
                })
                .intersperse_with(|| "\u{2228}".to_owned())
                .collect(),
            Self::Conj(l) => l
                .iter()
                .map(|fo| {
                    paren!(
                        matches!(*fo, Self::Disj(_) | Self::Imply(_, _) | Self::Equiv(_, _)),
                        fo.translate(varmap)
                    )
                })
                .intersperse_with(|| "\u{2227}".to_owned())
                .collect(),
            Self::Imply(x, y) => {
                let l = paren!(matches!(**x, Self::Imply(_, _)), x.translate(varmap));
                let r = paren!(matches!(**y, Self::Imply(_, _)), y.translate(varmap));
                format!("{} -> {}", l, r)
            }
            Self::Equiv(x, y) => {
                let l = paren!(matches!(**x, Self::Imply(_, _)), x.translate(varmap));
                let r = paren!(matches!(**y, Self::Imply(_, _)), y.translate(varmap));
                format!("{} = {}", l, r)
            }
            Self::Var(v) => varmap.get_var(*v).unwrap_or("unknown").to_owned(),
        }
    }
}
