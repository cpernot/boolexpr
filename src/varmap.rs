use std::collections::BTreeMap;
use std::rc::Rc;
#[derive(Clone, Default, Debug)]
pub struct VariableMap {
    var2int: BTreeMap<Rc<String>, u32>,
    int2var: BTreeMap<u32, Rc<String>>,
}
impl VariableMap {
    pub fn insert(&mut self, var: String) -> u32 {
        let max = self.int2var.last_key_value().map(|(k, _)| *k).unwrap_or(0);
        let code = (max + 1..)
            .find(|c| !self.int2var.contains_key(c))
            .expect("Wtf touts les u32 ont été pris ?");
        let var = Rc::new(var);
        self.var2int.insert(Rc::clone(&var), code);
        self.int2var.insert(code, var);
        code
    }
    pub fn get_code_or_insert(&mut self, var: &str) -> u32 {
        let var = Rc::new(var.to_owned());
        self.var2int
            .get(&var)
            .copied()
            .unwrap_or_else(|| self.insert(var.as_str().to_owned()))
    }
    pub fn get_var(&self, code: u32) -> Option<&str> {
        self.int2var.get(&code).map(|rc| rc.as_str())
    }
}
