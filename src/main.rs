#![feature(iter_intersperse)]

use clap::{Parser, ValueEnum};
use formule::Formule;

#[macro_use]
extern crate lalrpop_util;

#[derive(Clone, ValueEnum)]
enum Action {
    Tautologie,
    Simplifier,
    Satisfiable,
}
impl std::fmt::Display for Action {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let s = match self {
            Action::Tautologie => "Tautologie ? (+ valuation)",
            Action::Satisfiable => "Satisfiable ? (+ valuation)",
            Action::Simplifier => "Simplifier",
        };
        write!(f, "{}", s)
    }
}

#[derive(Parser)]
struct Args {
    #[arg(short, long, help = "Lire la formule dans un fichier")]
    file: Option<String>,
    #[arg(long = "action")]
    action: Option<Action>,
}

mod formule;
mod input;
mod quine;
mod rawformule;
mod valuation;
mod varmap;

lalrpop_mod!(pub grammar);

fn main() -> Result<(), failure::Error> {
    let args = Args::parse();
    let f = if let Some(path) = args.file {
        std::fs::read_to_string(path)?.parse::<Formule>()?
    } else {
        input::ask_formule()?
    }
    .simplify();
    let action = if let Some(a) = args.action {
        a
    } else {
        input::ask_action()?
    };
    match action {
        Action::Satisfiable => {
            if let Some(val) = f.satisfiable() {
                println!("Valuation: {}", val);
            } else {
                println!("Aucune valuation ne correspond");
            }
        }
        Action::Simplifier => {
            println!("Forme simplifiée: {}", f);
        }
        Action::Tautologie => {
            if let Some(val) = (!f).simplify().satisfiable() {
                println!("La formule n'est pas une tautologie: {}", val);
            } else {
                println!("La formule est une tautologie");
            }
        }
    }
    Ok(())
}
