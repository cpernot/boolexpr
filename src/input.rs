use inquire::{CustomType, Select};

use crate::formule::Formule;
use crate::Action;

pub(crate) fn ask_formule() -> Result<Formule, inquire::InquireError> {
    CustomType::<Formule>::new("Entrez une formule: ").prompt()
}
pub(crate) fn ask_action() -> Result<Action, inquire::InquireError> {
    Select::new(
        "Que voulez-vous faire de cette formule ?",
        [Action::Tautologie, Action::Simplifier, Action::Satisfiable].into(),
    )
    .prompt()
}
