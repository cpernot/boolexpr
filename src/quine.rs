use std::collections::BTreeSet;

use crate::rawformule::RawFormule;
use crate::valuation::Valuation;
struct Quine {
    unused: BTreeSet<u32>,
}
impl Quine {
    // Renvoie une valuation qui satisfait la formule.
    // La structure est inchangée
    fn quine(&mut self, formule: RawFormule) -> Option<Valuation> {
        match formule.simplify() {
            RawFormule::Top => Some(Valuation::default()),
            RawFormule::Bottom => None,
            f => {
                match self.unused.pop_first() {
                    Some(var) => {
                        let f1 = f.clone().replace(var, &RawFormule::Bottom).simplify();
                        let f2 = f.replace(var, &RawFormule::Top).simplify();
                        let result = if let Some(mut v) = self.quine(f1) {
                            v.set(var, false);
                            Some(v)
                        } else if let Some(mut v) = self.quine(f2) {
                            v.set(var, true);
                            Some(v)
                        } else {
                            None
                        };
                        self.unused.insert(var);
                        result
                    }
                    // Plus de variables à fixer: on évalue simplement l'expression.
                    None => {
                        if f.eval(&Valuation::default()).unwrap() {
                            Some(Valuation::default()) // Toute valuation satisfait l'expression
                        } else {
                            None // Impossible à satisfaire
                        }
                    }
                }
            }
        }
    }
}
pub fn solve(formule: RawFormule) -> Option<Valuation> {
    let mut v = BTreeSet::new();
    formule.list_vars(&mut v);
    let mut q = Quine { unused: v };
    q.quine(formule).map(|valu| {
        q.unused.into_iter().fold(valu, |mut v, var| {
            if v.get(var).is_none() {
                v.set(var, false);
            }
            v
        })
    })
}
