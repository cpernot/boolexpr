use std::collections::BTreeMap;

use crate::varmap::VariableMap;

const TOP: char = '\u{22A4}';
const BOTTOM: char = '\u{22A5}';
#[derive(Clone, Debug, Default)]
pub struct Valuation {
    vars: BTreeMap<u32, bool>,
}
#[derive(Clone, Debug, Default)]
pub struct TranslatedValuation {
    vars: BTreeMap<String, bool>,
}

impl Valuation {
    pub fn get(&self, var: u32) -> Option<bool> {
        self.vars.get(&var).copied()
    }
    pub fn set(&mut self, var: u32, value: bool) {
        self.vars.insert(var, value);
    }
    pub fn unset(&mut self, var: u32) {
        self.vars.remove(&var);
    }
    // Renvoie la valuation avec les noms des variables remis à leur place.
    pub fn translate(&self, map: &VariableMap) -> TranslatedValuation {
        let vars = self
            .vars
            .iter()
            .map(|(&c, &b)| {
                (
                    map.get_var(c)
                        .map(|s| s.to_owned())
                        .unwrap_or_else(|| "unknown".to_owned()),
                    b,
                )
            })
            .collect();
        TranslatedValuation { vars }
    }
}
impl std::fmt::Display for TranslatedValuation {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let s = self
            .vars
            .iter()
            .map(|(k, &v)| format!("{} -> {}", k, if v { TOP } else { BOTTOM }))
            .intersperse_with(|| ", ".into())
            .collect::<String>();
        f.write_str(&s)
    }
}
